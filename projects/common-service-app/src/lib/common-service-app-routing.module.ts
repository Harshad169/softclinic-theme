import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EntryComponent } from './components/entry-component';
import { AppointmentsComponent } from "./components/appointments/appointments.component";
import { AppointmentDetailComponent } from './components/appointments/appointments-detail.component';
import { LocationsComponent } from "./components/locations/locations.component";
import { LocationDetailComponent } from './components/locations/location-detail.component';

import { CommissionsComponent } from "./components/commissions/commissions.component";
import { DepartmentsComponent } from "./components/departments/departments.component";
import { FailitysComponent } from "./components/failitys/failitys.component";
import { InvoicesComponent } from "./components/invoices/invoices.component";
import { ServiceUsagesComponent } from "./components/service-usages/service-usages.component";
import { ServicesComponent } from "./components/services/services.component";
import { TpaMastersComponent } from "./components/tpa-masters/tpa-masters.component";
import { VendorsComponent } from "./components/vendors/vendors.component";
import { VisitsComponent } from "./components/visits/visits.component";

const routes: Routes = [
  {
    path: "",
    component: EntryComponent,
    children: [
      {
        path: "appointments",
        component: AppointmentsComponent,
        data: { extraParameter: "materialButtonsIndicators" }
      },
      {
        path: "appointments/create",
        component: AppointmentDetailComponent,
        // canActivate: [AuthGuard],
        data: { extraParameter: "materialButtonsIndicators" }
      },
      {
        path: "appointments/edit/:id",
        component: AppointmentDetailComponent,
        // canActivate: [AuthGuard],
        data: { extraParameter: "materialButtonsIndicators" }
      },
      {
        path: "locations",
        component: LocationsComponent,
        data: { extraParameter: "materialButtonsIndicators" }
      },
      {
        path: "locations/create",
        component: LocationDetailComponent,
        // canActivate: [AuthGuard],
        data: { extraParameter: "materialButtonsIndicators" }
      },
      {
        path: "locations/edit/:id",
        component: LocationDetailComponent,
        // canActivate: [AuthGuard],
        data: { extraParameter: "materialButtonsIndicators" }
      },

      // { path: "commissions", component: CommissionsComponent },

      // { path: "departments", component: DepartmentsComponent },

      // { path: "facilitys", component: FailitysComponent },

      // { path: "invoices", component: InvoicesComponent },

      // { path: "service-usages", component: ServiceUsagesComponent },

      // { path: "services", component: ServicesComponent },

      // { path: "tpa-masters", component: TpaMastersComponent },

      // { path: "vendors", component: VendorsComponent },

      // { path: "visits", component: VisitsComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommonServiceAppRoutingModule { }

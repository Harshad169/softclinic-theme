import {
  NgModule,
  Optional,
  SkipSelf,
  ModuleWithProviders,
} from "@angular/core";

import { PckApiModule } from "pck-api";
import { PckGeneralComponentsModule } from "pck-general-components";

import { CommonServiceAppRoutingModule } from "./common-service-app-routing.module";

import { LocationsComponent } from "./components/locations/locations.component";
import { LocationDetailComponent } from "./components/locations/location-detail.component";
import { AppointmentDetailComponent } from "./components/appointments/appointments-detail.component";
import { AppointmentsComponent } from "./components/appointments/appointments.component";
import { VisitsComponent } from "./components/visits/visits.component";
import { VendorsComponent } from "./components/vendors/vendors.component";
import { VisitsDetailComponent } from "./components/visits/visits-detail.component";
import { VendosDetailComponent } from "./components/vendors/vendos-detail.component";
import { CommissionsComponent } from "./components/commissions/commissions.component";
import { CommissionsDetailComponent } from "./components/commissions/commissions-detail.component";
import { FacilitysDetailComponent } from "./components/failitys/facilitys-detail.component";
import { FailitysComponent } from "./components/failitys/failitys.component";
import { InvoicesComponent } from "./components/invoices/invoices.component";
import { InvoicesDetailComponent } from "./components/invoices/invoices-detail.component";
import { ServiceUsagesComponent } from "./components/service-usages/service-usages.component";
import { ServiceUsagesDetailComponent } from "./components/service-usages/service-usages-detail.component";
import { ServicesComponent } from "./components/services/services.component";
import { ServicesDetailComponent } from "./components/services/services-detail.component";
import { TpaMastersComponent } from "./components/tpa-masters/tpa-masters.component";
import { TpaMastersDetailComponent } from "./components/tpa-masters/tpa-masters-detail.component";
import { DepartmentsComponent } from "./components/departments/departments.component";
import { DepartmentsDetailComponent } from "./components/departments/departments-detail.component";

import { LocationService } from "./services/location-service";
import { AppointmentService } from "./services/appointment-service";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EntryComponent } from './components/entry-component';

@NgModule({
  imports: [
    CommonServiceAppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PckApiModule.forChild(),
    PckGeneralComponentsModule.forChild(),
  ],
  declarations: [
    EntryComponent,
    LocationsComponent,
    LocationDetailComponent,
    AppointmentsComponent,
    AppointmentDetailComponent,
    VisitsComponent,
    VisitsDetailComponent,
    VendorsComponent,
    VendosDetailComponent,
    CommissionsComponent,
    CommissionsDetailComponent,
    FailitysComponent,
    FacilitysDetailComponent,
    InvoicesComponent,
    InvoicesDetailComponent,
    ServiceUsagesComponent,
    ServiceUsagesDetailComponent,
    ServicesComponent,
    ServicesDetailComponent,
    TpaMastersComponent,
    TpaMastersDetailComponent,
    DepartmentsComponent,
    DepartmentsDetailComponent
  ],
  providers: [
    LocationService,
    AppointmentService
  ],
  exports: [
  ]
})
export class CommonServiceAppModule {
  constructor(@Optional() @SkipSelf() parentModule: CommonServiceAppModule) {
    if (parentModule) console.log("CommonServiceAppModule loaded");
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CommonServiceAppModule,
      providers: [
        LocationService,
        AppointmentService
      ]
    };
  }

  static forChild(): ModuleWithProviders {
    return {
      ngModule: CommonServiceAppModule,
    };
  }
}

import { Component, OnInit } from '@angular/core';
import { AppointmentModel } from '../../models/appointment-model';
import { AppointmentService } from '../../services/appointment-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cmn-appointments-detail',
  templateUrl: './appointments-detail.component.html',
  styleUrls: ['./appointments-detail.component.scss']
})
export class AppointmentDetailComponent implements OnInit {

  item: AppointmentModel = new AppointmentModel();

  constructor(
    private appointmentService: AppointmentService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      var id = params["id"];
      if (id) {
        this.appointmentService.get(id).then(response => {
          // this.item = response;
        });
      }
    });
  }

  save() {
    if (this.item.id) {
      // this.appointmentService.update(this.item).then(response => {
      //   this.backToList();
      // });
    } else {
      // this.appointmentService.create(this.item).then(response => {
      //   this.backToList();
      // });
    }
  }

  onBackClick() {
    this.backToList();
  }

  private backToList() {
    this.router.navigate(["common/appointments"]);
  }
}

import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import {
  SCTableComponent,
  DataSource,
  TableColumn,
  TableActionColumn,
} from "pck-general-components";
import { AppointmentService } from "../../services/appointment-service";
import { Router } from "@angular/router";
import { AppointmentModel } from "../../models/appointment-model";
// import { Filter } from "pck-api";
@Component({
  selector: "cmn-appointments",
  templateUrl: "./appointments.component.html",
  styleUrls: ["./appointments.component.scss"],
})
export class AppointmentsComponent {
  @ViewChild("listTable", { static: true })
  listTable: SCTableComponent;
  public dataSource: DataSource<AppointmentModel> = new DataSource<
    AppointmentModel
  >();
  showIdColumn: boolean = true;

  constructor(
    private appointmentService: AppointmentService,
    private router: Router // private translationService: TranslationService, // private translation: ResourceTranslationService
  ) { }

  ngOnInit() {
    this.prepareDataSource();
  }

  private prepareDataSource() {
    this.dataSource.bind(
      (filter) =>
        new Promise((resolve, reject) => {
          this.appointmentService
            .getAppointments(filter)
            .then((response) => {
              resolve(response);
            })
            .catch((reason) => {
              reject(reason);
            });
        })
    );
  }
  public infodetailColumns: TableColumn[] = [
    {
      label: "PatientName",
      name: "patientName",
      orderBy: "patientName",
      isFilterAllowed: true,
      isDefaultColumn: true,
      visible: true,
    },

    {
      label: "DoctorName",
      name: "doctorName",
      orderBy: "doctorName",
      isFilterAllowed: false,
      isDefaultColumn: true,
      visible: true,
    },
    {
      label: "TimeSlot",
      name: "timeSlot",
      orderBy: "timeSlot",
      isFilterAllowed: false,
      isDefaultColumn: true,
      visible: true,
    },
    {
      label: "Status",
      name: "statusString",
      orderBy: "statusString",
      isFilterAllowed: false,
      isDefaultColumn: true,
      visible: true,
    },
    //{ label: "Status", name: "statusString", visible: true, orderBy: "", cssClass: "col-md-4", isDetail: true, isDefaultColumn: false }
  ];
  public actionColumns: TableActionColumn[] = [
    {
      icon: "edit",
      title: "Edit",
      click: (row: any) => {
        this.redirectToEdit(row);
      },
    },
    {
      icon: "delete",
      title: "Delete",
      click: (row: any) => {
        this.deleteItem(row.id);
      },
    },
    { icon: "print", title: "Print" },
  ];
  // ngOnInit() { }

  create() {
    this.router.navigate(["common/appointments/create"]);
  }

  redirectToEdit(row: AppointmentModel) {
    this.router.navigate(["common/appointments/edit", row.id]);
  }

  deleteItem(id: string) {
    // this.appointmentService.remove(id).then(response => {
    //   this.listTable.refresh();
    // });
  }
}

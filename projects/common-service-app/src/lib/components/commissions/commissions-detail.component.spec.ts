import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionsDetailComponent } from './commissions-detail.component';

describe('CommissionsDetailComponent', () => {
  let component: CommissionsDetailComponent;
  let fixture: ComponentFixture<CommissionsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

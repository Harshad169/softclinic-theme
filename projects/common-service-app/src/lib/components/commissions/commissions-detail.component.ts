import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-commissions-detail',
  templateUrl: './commissions-detail.component.html',
  styleUrls: ['./commissions-detail.component.scss']
})
export class CommissionsDetailComponent implements OnInit {

  ngOnInit(): void {
  }

  // item: CommissionDetailModel = new CommissionDetailModel();
  // constructor(
  //   private commissionDetailService: CommissionDetailService,
  //   private router: Router,
  //   private route: ActivatedRoute
  // ) { }

  // ngOnInit() {
  //   this.route.params.subscribe(params => {
  //     var id = params["id"];
  //     if (id) {
  //       this.commissionDetailService.get(id).then(response => {
  //         this.item = response;
  //       });
  //     }
  //   });
  // }
  // save() {
  //   if (this.item.id) {
  //     this.commissionDetailService.update(this.item).then(response => {
  //       this.backToList();
  //     });
  //   } else {
  //     this.commissionDetailService.create(this.item).then(response => {
  //       this.backToList();
  //     });
  //   }
  // }

  // onBackClick() {
  //   this.backToList();
  // }

  // private backToList() {
  //   this.router.navigate(["commission/list"]);
  // }

}

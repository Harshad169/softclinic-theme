import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-commissions',
  templateUrl: './commissions.component.html',
  styleUrls: ['./commissions.component.scss']
})
export class CommissionsComponent implements OnInit {

  ngOnInit(): void {
  }

  // @ViewChild("listTable", { static: false })
  // listTable: SCTableComponent;

  // showIdColumn: boolean = true;

  // constructor(private commissionDetailService: CommissionDetailService,
  //     private router: Router,
  //     private translationService: TranslationService,
  //     private translation: ResourceTranslationService,
  //     private helper: JwtHelperService) {
  // }

  // public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //     return this.commissionDetailService.getList(filter);
  // });
  // public infodetailColumns: TableColumn[] = [
  //     { label: "InvoiceType", name: "invoiceId", orderBy: "invoiceId", isFilterAllowed: true, isDefaultColumn: true, visible: true },
  //     { label: "VendorName", name: "vendorId", orderBy: "vendorId", isFilterAllowed: false, isDefaultColumn: true, visible: true },
  //     { label: "Amount", name: "amount", orderBy: "amount", isFilterAllowed: false, isDefaultColumn: true, visible: true },
  //     { label: "Notes", name: "notes", orderBy: "notes", isFilterAllowed: false, isDefaultColumn: true, visible: true },
  // ];

  // public actionColumns: TableActionColumn[] = [
  //     {
  //         icon: "edit", title: "Edit", click: (row: any) => {
  //             debugger;
  //             this.redirectToEdit(row);
  //         }
  //     },
  //     {
  //         icon: "delete", title: "Delete", click: (row: any) => {
  //             this.deleteItem(row.id);
  //         }
  //     },
  //     { icon: "print", title: "Print" },
  // ]

  // ngOnInit() {

  // }

  // create() {
  //     this.router.navigate(["commission/create"]);
  // }

  // redirectToEdit(row: CommissionDetailModel) {
  //     this.router.navigate(["commission/edit", row.id]);
  // }

  // deleteItem(id: string) {
  //     this.commissionDetailService.delete(id).then(response => {
  //         this.listTable.refresh();
  //     });
  // }

}

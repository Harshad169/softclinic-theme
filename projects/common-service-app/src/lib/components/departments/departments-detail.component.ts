import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-departments-detail',
  templateUrl: './departments-detail.component.html',
  styleUrls: ['./departments-detail.component.scss']
})
export class DepartmentsDetailComponent implements OnInit {

  ngOnInit(): void {
  }

  // item: DepartmentModel = new DepartmentModel();
  // constructor(
  //   private departmentService: DepartmentService,
  //   private router: Router,
  //   private route: ActivatedRoute
  // ) {}

  // ngOnInit() {
  //   this.route.params.subscribe(params => {
  //     var id = params["id"];
  //     if (id) {
  //       this.departmentService.get(id).then(response => {
  //         this.item = response;
  //       });
  //     }
  //   });
  // }
  // // onStatusChange(item) {
  // //   this.item.checked = !item.checked;
  // // }
  // save() {
  //   if (this.item.id) {
  //     this.departmentService.update(this.item).then(response => {
  //       this.backToList();
  //     });
  //   } else {
  //     this.departmentService.create(this.item).then(response => {
  //       this.backToList();
  //     });
  //   }
  // }

  // onBackClick() {
  //   this.backToList();
  // }

  // private backToList() {
  //   this.router.navigate(["department/list"]);
  // }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent implements OnInit {

  ngOnInit(): void {
  }

  // @ViewChild("listTable", { static: false })
  // listTable: SCTableComponent;

  // showIdColumn: boolean = true;

  // constructor(
  //   private departmentService: DepartmentService,
  //   private router: Router,
  //   private translationService: TranslationService,
  //   private translation: ResourceTranslationService
  // ) {}

  // public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //   return this.departmentService.getList(filter);
  // });
  // public infodetailColumns: TableColumn[] = [
  //   {
  //     label: "Name",
  //     name: "Name",
  //     orderBy: "Name",
  //     isFilterAllowed: true,
  //     isDefaultColumn: true,
  //     visible: true
  //   }
  //   //{ label: "Status", name: "statusString", visible: true, orderBy: "", cssClass: "col-md-4", isDetail: true, isDefaultColumn: false }
  // ];
  // public actionColumns: TableActionColumn[] = [
  //   {
  //     icon: "edit",
  //     title: "Edit",
  //     click: (row: any) => {
  //       this.redirectToEdit(row);
  //     }
  //   },
  //   {
  //     icon: "delete",
  //     title: "Delete",
  //     click: (row: any) => {
  //       this.deleteItem(row.id);
  //     }
  //   },
  //   { icon: "print", title: "Print" }
  // ];
  // ngOnInit() {}

  // create() {
  //   this.router.navigate(["department/create"]);
  // }

  // redirectToEdit(row: DepartmentModel) {
  //   this.router.navigate(["department/edit", row.id]);
  // }

  // deleteItem(id: string) {
  //   this.departmentService.delete(id).then(response => {
  //     this.listTable.refresh();
  //   });
  // }

}

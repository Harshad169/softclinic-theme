import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitysDetailComponent } from './facilitys-detail.component';

describe('FacilitysDetailComponent', () => {
  let component: FacilitysDetailComponent;
  let fixture: ComponentFixture<FacilitysDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitysDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitysDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

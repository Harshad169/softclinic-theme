import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FailitysComponent } from './failitys.component';

describe('FailitysComponent', () => {
  let component: FailitysComponent;
  let fixture: ComponentFixture<FailitysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FailitysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FailitysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

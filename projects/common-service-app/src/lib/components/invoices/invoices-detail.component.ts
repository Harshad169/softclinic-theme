import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-invoices-detail',
  templateUrl: './invoices-detail.component.html',
  styleUrls: ['./invoices-detail.component.scss']
})
export class InvoicesDetailComponent implements OnInit {

  ngOnInit(): void {
  }

  // item: InvoiceModel = new InvoiceModel();
  // constructor(private invoiceService: InvoiceService, private router: Router, private route: ActivatedRoute) {
  // }

  // ngOnInit() {
  //     this.route.params.subscribe(params => {
  //         var id = params["id"];
  //         if (id) {
  //             this.invoiceService.get(id).then(response => {
  //                 this.item = response;
  //             });
  //         }
  //     });
  // }

  // save() {
  //     if (this.item.id) {
  //         this.invoiceService.update(this.item).then(response => {
  //             this.backToList();
  //         });
  //     } else {
  //         this.invoiceService.create(this.item).then(response => {
  //             this.backToList();
  //         });
  //     }
  // }

  // onBackClick() {
  //     this.backToList();
  // }

  // private backToList() {
  //     this.router.navigate(["invoice/list"]);
  // }
}

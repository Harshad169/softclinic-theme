import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

  ngOnInit(): void {
  }

  //  @ViewChild("listTable", { static: false })
  //     listTable: SCTableComponent;

  //     showIdColumn: boolean = true;

  //     constructor(private invoiceService: InvoiceService, private router: Router, private translationService: TranslationService, private translation: ResourceTranslationService) {
  //     }

  //     public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //         return this.invoiceService.getList(filter);
  //     });

  //     ngOnInit() {

  //     }

  //     create() {
  //         this.router.navigate(["invoice/create"]);
  //     }

  //     redirectToEdit(row: InvoiceModel) {
  //         this.router.navigate(["invoice/edit", row.id]);
  //     }

  //     deleteItem(event: Event, id: string) {
  //         event.preventDefault();
  //         event.stopPropagation();
  //         if (event.bubbles)
  //             event.cancelBubble = true;

  //         this.invoiceService.delete(id).then(response => {
  //             this.listTable.refresh();
  //         });
  //     }
}

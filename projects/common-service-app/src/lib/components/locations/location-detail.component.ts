import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationService } from '../../services/location-service';
import { LocationRequestModel } from '../../models/location-request-model';

@Component({
  selector: 'cmn-locations-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.scss']
})
export class LocationDetailComponent implements OnInit {

  ngOnInit() { }

  // locationformgroup: FormGroup;

  item: LocationRequestModel = new LocationRequestModel();
  // locationlist: CommonDropdownModel[] = [];
  // LocationTypedata: CommonDropdownModel[] = [];

  constructor(
    private locationService: LocationService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    //this.item.ParentLocation = "";
  }

  // public medicinesDatasource: Datasource = new Datasource(
  //   (filter: FilterModel) => {
  //     return this.locationService.getList(filter);
  //   }
  // );

  // public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //   filter.perPage = 0;
  //   filter.page = 0;
  //   var obj = this.locationService.getList(filter);
  //   debugger;
  //   return obj;
  // });

  // ngOnInit() {
  //   this.locationformgroup = new FormGroup({
  //     LocationCode: new FormControl("", Validators.required)
  //     //lastName: new FormControl("", Validators.required)
  //   });

  //   this.locationService.getdropdownlist().then(response => {
  //     this.locationlist = response;
  //     debugger;
  //   });

  //   this.locationService.getlocationtypelist().then(response => {
  //     this.LocationTypedata = response;
  //     debugger;
  //   });

  //   this.route.params.subscribe(params => {
  //     var id = params["id"];
  //     if (id) {
  //       this.locationService.get(id).then(response => {
  //         debugger;
  //         this.item = response;
  //       });
  //     }
  //   });
  // }
  save() {
    // debugger;
    // if (this.item.id) {
    //   this.locationService.update(this.item).then(response => {
    //     alert(response);
    //     this.backToList();
    //   });
    // } else {
    //   this.locationService.create(this.item).then(response => {
    //     alert(response);
    //     this.backToList();
    //   });
    // }
  }

  onBackClick() {
    this.backToList();
  }

  private backToList() {
    this.router.navigate(["common/locations"]);
  }
}

import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { LocationModel } from "../../models/location-model";
import { LocationService } from "../../services/location-service";
import {
  SCTableComponent,
  DataSource,
  TableColumn,
  TableActionColumn,
  Filter,
} from "pck-general-components";

@Component({
  selector: "cmn-locations",
  templateUrl: "./locations.component.html",
  styleUrls: ["./locations.component.scss"],
})
export class LocationsComponent {
  @ViewChild("listTable", { static: true })
  listTable: SCTableComponent;
  public dataSource: DataSource<LocationModel> = new DataSource<
    LocationModel
  >();
  showIdColumn: boolean = true;

  public infodetailColumns: TableColumn[] = [
    {
      label: "name",
      name: "name",
      orderBy: "name",
      isFilterAllowed: true,
      isDefaultColumn: true,
      visible: true,
    },
    {
      label: "locationCode",
      name: "code",
      orderBy: "code",
      isFilterAllowed: true,
      isDefaultColumn: true,
      visible: true,
    },
    {
      label: "locationType",
      name: "typeId",
      orderBy: "typeId",
      isFilterAllowed: true,
      isDefaultColumn: true,
      visible: true,
    },
    {
      label: "ParentLocation",
      name: "parentId",
      orderBy: "parentId",
      isFilterAllowed: true,
      isDefaultColumn: true,
      visible: true,
    },
  ];

  public actionColumns: TableActionColumn[] = [
    {
      icon: "edit",
      title: "Edit",
      click: (row: any) => {
        this.redirectToEdit(row);
      },
    },
    {
      icon: "delete",
      title: "Delete",
      click: (row: any) => {
        this.deleteItem(row.id);
      },
    },
    { icon: "print", title: "Print" },
  ];

  constructor(
    private locationService: LocationService,
    private router: Router // private translationService: TranslationService,
  ) // private translation: ResourceTranslationService
  { }

  ngOnInit() {
    this.prepareDataSource();
  }

  private prepareDataSource() {
    this.dataSource.bind(
      (filter) =>
        new Promise((resolve, reject) => {
          this.locationService
            .getLocations(filter)
            .then((response) => {
              resolve(response);
            })
            .catch((reason) => {
              reject(reason);
            });
        })
    );
  }

  create() {
    this.router.navigate(["common/locations/create"]);
  }

  redirectToEdit(row: LocationModel) {
    this.router.navigate(["common/locations/edit", row.id]);
  }

  deleteItem(id: string) {
    this.locationService.remove(id).then((response) => {
      this.listTable.refresh();
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceUsagesDetailComponent } from './service-usages-detail.component';

describe('ServiceUsagesDetailComponent', () => {
  let component: ServiceUsagesDetailComponent;
  let fixture: ComponentFixture<ServiceUsagesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceUsagesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceUsagesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-service-usages-detail',
  templateUrl: './service-usages-detail.component.html',
  styleUrls: ['./service-usages-detail.component.scss']
})
export class ServiceUsagesDetailComponent implements OnInit {

  ngOnInit(): void {
  }

  // item: ServiceUsageModel = new ServiceUsageModel();
  // constructor(
  //   private serviceusageService: ServiceUsageService,
  //   private router: Router,
  //   private route: ActivatedRoute
  // ) {}

  // ngOnInit() {
  //   this.route.params.subscribe(params => {
  //     var id = params["id"];
  //     if (id) {
  //       this.serviceusageService.get(id).then(response => {
  //         this.item = response;
  //       });
  //     }
  //   });
  // }

  // save() {
  //   if (this.item.id) {
  //     this.serviceusageService.update(this.item).then(response => {
  //       this.backToList();
  //     });
  //   } else {
  //     this.serviceusageService.create(this.item).then(response => {
  //       this.backToList();
  //     });
  //   }
  // }

  // onBackClick() {
  //   this.backToList();
  // }

  // private backToList() {
  //   this.router.navigate(["serviceusage/list"]);
  // }
}

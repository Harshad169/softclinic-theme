import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceUsagesComponent } from './service-usages.component';

describe('ServiceUsagesComponent', () => {
  let component: ServiceUsagesComponent;
  let fixture: ComponentFixture<ServiceUsagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceUsagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceUsagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

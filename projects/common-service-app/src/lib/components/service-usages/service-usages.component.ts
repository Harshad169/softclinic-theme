import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-service-usages',
  templateUrl: './service-usages.component.html',
  styleUrls: ['./service-usages.component.scss']
})
export class ServiceUsagesComponent implements OnInit {

  // @ViewChild("listTable", { static: false })
  // listTable: SCTableComponent;

  // showIdColumn: boolean = true;

  // constructor(
  //   private serviceusageService: ServiceUsageService,
  //   private router: Router,
  //   private translationService: TranslationService,
  //   private translation: ResourceTranslationService
  // ) {}

  // public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //   return this.serviceusageService.getList(filter);
  // });
  // public infodetailColumns: TableColumn[] = [
  //   {
  //     label: "VisitType",
  //     name: "visitType",
  //     orderBy: "visitType",
  //     isFilterAllowed: true,
  //     isDefaultColumn: true,
  //     visible: true
  //   },
  //   {
  //     label: "PatientName",
  //     name: "patientName",
  //     orderBy: "patientName",
  //     isFilterAllowed: true,
  //     isDefaultColumn: true,
  //     visible: true
  //   },
  //   {
  //     label: "Investigation",
  //     name: "investigations",
  //     orderBy: "investigations",
  //     isFilterAllowed: false,
  //     isDefaultColumn: true,
  //     visible: true
  //   }
  //   //{ label: "Status", name: "statusString", visible: true, orderBy: "", cssClass: "col-md-4", isDetail: true, isDefaultColumn: false }
  // ];
  // public actionColumns: TableActionColumn[] = [
  //   {
  //     icon: "edit",
  //     title: "Edit",
  //     click: (row: any) => {
  //       debugger;
  //       this.redirectToEdit(row);
  //     }
  //   },
  //   {
  //     icon: "delete",
  //     title: "Delete",
  //     click: (row: any) => {
  //       this.deleteItem(row.id);
  //     }
  //   },
  //   { icon: "print", title: "Print" }
  // ];
  ngOnInit() { }

  // create() {
  //   this.router.navigate(["serviceusage/create"]);
  // }

  // redirectToEdit(row: ServiceUsageModel) {
  //   this.router.navigate(["serviceusage/edit", row.id]);
  // }

  // deleteItem(id: string) {
  //   this.serviceusageService.delete(id).then(response => {
  //     this.listTable.refresh();
  //   });
  // }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-services-detail',
  templateUrl: './services-detail.component.html',
  styleUrls: ['./services-detail.component.scss']
})
export class ServicesDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  // item: ServiceModel = new ServiceModel();
  //   constructor(private serviceService: ServiceService, private router: Router, private route: ActivatedRoute) {
  //   }

  //   ngOnInit() {
  //       this.route.params.subscribe(params => {
  //           var id = params["id"];
  //           if (id) {
  //               this.serviceService.get(id).then(response => {
  //                   this.item = response;
  //               });
  //           }
  //       });
  //   }

  //   save() {
  //       if (this.item.id) {
  //           this.serviceService.update(this.item).then(response => {
  //               this.backToList();
  //           });
  //       } else {
  //           this.serviceService.create(this.item).then(response => {
  //               this.backToList();
  //           });
  //       }
  //   }

  //   onBackClick() {
  //       this.backToList();
  //   }

  //   private backToList() {
  //       this.router.navigate(["service/list"]);
  //   }
}

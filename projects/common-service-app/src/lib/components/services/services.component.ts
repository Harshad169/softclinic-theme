import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  // @ViewChild("listTable", { static: false })
  // listTable: SCTableComponent;

  // showIdColumn: boolean = true;

  // constructor(private serviceService: ServiceService, private router: Router, private translationService: TranslationService, private translation: ResourceTranslationService) {
  // }

  // public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //     return this.serviceService.getList(filter);
  // });

  ngOnInit() {

  }

  // create() {
  //     this.router.navigate(["service/create"]);
  // }

  // redirectToEdit(row: ServiceModel) {
  //     this.router.navigate(["service/edit", row.id]);
  // }

  // deleteItem(event: Event, id: string) {
  //     event.preventDefault();
  //     event.stopPropagation();
  //     if (event.bubbles)
  //         event.cancelBubble = true;

  //     this.serviceService.delete(id).then(response => {
  //         this.listTable.refresh();
  //     });
  // }
}

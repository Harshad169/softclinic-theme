import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaMastersDetailComponent } from './tpa-masters-detail.component';

describe('TpaMastersDetailComponent', () => {
  let component: TpaMastersDetailComponent;
  let fixture: ComponentFixture<TpaMastersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaMastersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaMastersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

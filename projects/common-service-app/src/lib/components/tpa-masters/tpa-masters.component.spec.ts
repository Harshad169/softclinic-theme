import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaMastersComponent } from './tpa-masters.component';

describe('TpaMastersComponent', () => {
  let component: TpaMastersComponent;
  let fixture: ComponentFixture<TpaMastersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaMastersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaMastersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

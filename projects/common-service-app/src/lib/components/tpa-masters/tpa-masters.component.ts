import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-tpa-masters',
  templateUrl: './tpa-masters.component.html',
  styleUrls: ['./tpa-masters.component.scss']
})
export class TpaMastersComponent implements OnInit {
  // @ViewChild("listTable", { static: false })
  // listTable: SCTableComponent;

  // showIdColumn: boolean = true;

  // constructor(
  //   private tpaMasterService: TPAMasterService,
  //   private router: Router,
  //   private translationService: TranslationService,
  //   private translation: ResourceTranslationService
  // ) {}

  // public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //   return this.tpaMasterService.getList(filter);
  // });
  // public infodetailColumns: TableColumn[] = [
  //   {
  //     label: "Name",
  //     name: "name",
  //     orderBy: "name",
  //     isFilterAllowed: true,
  //     isDefaultColumn: true,
  //     visible: true
  //   },

  //   {
  //     label: "DisplayName",
  //     name: "displayName",
  //     orderBy: "displayName",
  //     isFilterAllowed: false,
  //     isDefaultColumn: true,
  //     visible: true
  //   },
  //   {
  //     label: "ContactPersonEmail",
  //     name: "contactPersonEmail",
  //     orderBy: "contactPersonEmail",
  //     isFilterAllowed: false,
  //     isDefaultColumn: true,
  //     visible: true
  //   },
  //   {
  //     label: "ContactPersonPhone",
  //     name: "contactPersonPhone",
  //     visible: true,
  //     orderBy: "",
  //     cssClass: "col-md-4",
  //     isDetail: true,
  //     isDefaultColumn: false
  //   },
  //   {
  //     label: "Address",
  //     name: "address",
  //     visible: true,
  //     orderBy: "",
  //     cssClass: "col-md-4",
  //     isDetail: true,
  //     isDefaultColumn: false
  //   }

  //   //{ label: "Status", name: "statusString", visible: true, orderBy: "", cssClass: "col-md-4", isDetail: true, isDefaultColumn: false }
  // ];
  // public actionColumns: TableActionColumn[] = [
  //   {
  //     icon: "edit",
  //     title: "Edit",
  //     click: (row: any) => {
  //       this.redirectToEdit(row);
  //     }
  //   },
  //   {
  //     icon: "delete",
  //     title: "Delete",
  //     click: (row: any) => {
  //       this.deleteItem(row.id);
  //     }
  //   },
  //   { icon: "print", title: "Print" }
  // ];
  ngOnInit() { }

  // create() {
  //   this.router.navigate(["tpaMaster/create"]);
  // }

  // redirectToEdit(row: TPAMasterModel) {
  //   this.router.navigate(["tpaMaster/edit", row.id]);
  // }

  // deleteItem(id: string) {
  //   this.tpaMasterService.delete(id).then(response => {
  //     this.listTable.refresh();
  //   });
  // }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent implements OnInit {

  // @ViewChild("listTable", { static: false })
  //   listTable: SCTableComponent;

  //   showIdColumn: boolean = true;

  //   constructor(private visitService: VendorService, private router: Router, private translationService: TranslationService, private translation: ResourceTranslationService) {
  //   }

  //   public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //       return this.visitService.getList(filter);
  //   });

  ngOnInit() {

  }

  //   create() {
  //       this.router.navigate(["vendor/create"]);
  //   }

  //   redirectToEdit(row: VendorModel) {
  //       this.router.navigate(["vendor/edit", row.id]);
  //   }

  //   deleteItem(event: Event, id: string) {
  //       event.preventDefault();
  //       event.stopPropagation();
  //       if (event.bubbles)
  //           event.cancelBubble = true;

  //       this.visitService.delete(id).then(response => {
  //           this.listTable.refresh();
  //       });
  //   }

}

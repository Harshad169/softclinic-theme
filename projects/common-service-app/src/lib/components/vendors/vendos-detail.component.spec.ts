import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendosDetailComponent } from './vendos-detail.component';

describe('VendosDetailComponent', () => {
  let component: VendosDetailComponent;
  let fixture: ComponentFixture<VendosDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendosDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendosDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-vendos-detail',
  templateUrl: './vendos-detail.component.html',
  styleUrls: ['./vendos-detail.component.scss']
})
export class VendosDetailComponent implements OnInit {

  ngOnInit(): void {
  }

  // item: VendorModel = new VendorModel();
  // constructor(private vendorService: VendorService, private router: Router, private route: ActivatedRoute) {
  // }

  // ngOnInit() {
  //     this.route.params.subscribe(params => {
  //         var id = params["id"];
  //         if (id) {
  //             this.vendorService.get(id).then(response => {
  //                 this.item = response;
  //             });
  //         }
  //     });
  // }

  // save() {
  //     if (this.item.id) {
  //         this.vendorService.update(this.item).then(response => {
  //             this.backToList();
  //         });
  //     } else {
  //         this.vendorService.create(this.item).then(response => {
  //             this.backToList();
  //         });
  //     }
  // }

  // onBackClick() {
  //     this.backToList();
  // }

  // private backToList() {
  //     this.router.navigate(["vendor/list"]);
  // }
}

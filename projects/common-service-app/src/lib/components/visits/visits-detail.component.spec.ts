import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitsDetailComponent } from './visits-detail.component';

describe('VisitsDetailComponent', () => {
  let component: VisitsDetailComponent;
  let fixture: ComponentFixture<VisitsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-visits-detail',
  templateUrl: './visits-detail.component.html',
  styleUrls: ['./visits-detail.component.scss']
})
export class VisitsDetailComponent implements OnInit {

  ngOnInit(): void {
  }
  // item: VisitModel = new VisitModel();
  // constructor(private visitService: VisitService, private router: Router, private route: ActivatedRoute) {
  // }

  // ngOnInit() {
  //     this.route.params.subscribe(params => {
  //         var id = params["id"];
  //         if (id) {
  //             this.visitService.get(id).then(response => {
  //                 this.item = response;
  //             });
  //         }
  //     });
  // }

  // save() {
  //     if (this.item.id) {
  //         this.visitService.update(this.item).then(response => {
  //             this.backToList();
  //         });
  //     } else {
  //         this.visitService.create(this.item).then(response => {
  //             this.backToList();
  //         });
  //     }
  // }

  // onBackClick() {
  //     this.backToList();
  // }

  // private backToList() {
  //     this.router.navigate(["visit/list"]);
  // }
}

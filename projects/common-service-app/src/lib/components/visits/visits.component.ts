import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmn-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.scss']
})
export class VisitsComponent implements OnInit {

  // @ViewChild("listTable", { static: false })
  // listTable: SCTableComponent;

  // showIdColumn: boolean = true;

  // constructor(private visitService: VisitService, private router: Router, private translationService: TranslationService, private translation: ResourceTranslationService) {
  // }

  // public datasource: Datasource = new Datasource((filter: FilterModel) => {
  //   return this.visitService.getList(filter);
  // });

  ngOnInit() {

  }

  // create() {
  //   this.router.navigate(["visit/create"]);
  // }

  // redirectToEdit(row: VisitModel) {
  //   this.router.navigate(["visit/edit", row.id]);
  // }

  // deleteItem(event: Event, id: string) {
  //   event.preventDefault();
  //   event.stopPropagation();
  //   if (event.bubbles)
  //     event.cancelBubble = true;

  //   this.visitService.delete(id).then(response => {
  //     this.listTable.refresh();
  //   });
  // }
}

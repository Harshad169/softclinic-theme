import { EntityModel } from 'pck-general-components';

export class AppointmentModel extends EntityModel
{
  PatientName: string;
  DoctorName: string;
  TimeSlot: Date;
  checked: boolean;
}

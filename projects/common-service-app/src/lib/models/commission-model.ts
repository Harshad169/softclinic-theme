import { EntityModel } from 'pck-general-components';

export class CommissionModel extends EntityModel
{
  InvoiceType: string;
  VendorName: string;
  Amount: number = 0;
  Notes: string;
}

export class FacilityAddressModel {
  FacilityId: string;
  PostBoxNumber: string;
  AddressLine1: string;
  AddressLine2: string;
  City: string;
  State: string;
  Country: string;
  Zip: string;
  EmailId: string;
  PhoneNumber1: string;
  PhoneNumber2: string;
  FaxNumber: string;
}

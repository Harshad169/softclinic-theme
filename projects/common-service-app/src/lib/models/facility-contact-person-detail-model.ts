export class FacilityContactPersonDetailModel {
  FacilityId: string;
  FacilityInCharge: string;
  FacilityShortName: string;
  EmailId: string;
  PhoneNumber: string;
  InchargeDesignation: string;
}

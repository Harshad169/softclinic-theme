import { FacilityAddressModel } from "./facility-address-model";
import { FacilityAffiliationOrgDetailModel } from "./facility-affiliation-org-detail-model";
import { FacilityContactPersonDetailModel } from "./facility-contact-person-detail-model";
import { EntityModel } from 'pck-general-components';

export class FacilityModel extends EntityModel
{
  checked: boolean;
  FacilityDisplayName: string;
  FacilityTypeId: string;
  FacilityShortName: string;
  Website: string;
  PostBoxNumber: string;
  LicenceNumber: string;
  FacilityLogo: string;
  WeeklyOffDay: string;
  SpecilitesTypeId: string;
  FacilityAddress: FacilityAddressModel;
  FacilityAffiliationOrgDetail: FacilityAffiliationOrgDetailModel;
  FacilityContactPersonDetail: FacilityContactPersonDetailModel;
}

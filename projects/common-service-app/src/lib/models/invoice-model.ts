import { EntityModel } from 'pck-general-components';

export class InvoiceModel extends EntityModel
{
  patientid: string;
  totalamount: string;
  grossdiscount: string;
  netvalue: string;
  netpatientvalue: string;
  tpaid: string;
}

import { EntityModel } from 'pck-general-components';

export class LocationModel extends EntityModel
{
  LocationCode: string;
  LocationType: string;
  ParentName: string;
}

import { EntityModel } from 'pck-general-components';

export class LocationRequestModel extends EntityModel
{
  name: string;
  code: string;
  typeId: string;
  parentId: string;
}

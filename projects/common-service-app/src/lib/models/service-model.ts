import { EntityModel } from 'pck-general-components';

export class ServiceModel extends EntityModel
{
  displayname: string;
  servicerelatedto: string;
  type: string;
}

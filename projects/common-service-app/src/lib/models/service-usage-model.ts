import { EntityModel } from 'pck-general-components';

export class ServiceUsageModel extends EntityModel
{
  VisitType: string;
  PatienName: string;
  Investigations: string;
}

import { EntityModel } from 'pck-general-components';

export class TpaMasterModel extends EntityModel
{
  ContactPersonEmail: string;
  ContactPersonPhone: string;
  Address: string;
}

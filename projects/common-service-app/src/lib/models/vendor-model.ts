import { EntityModel } from 'pck-general-components';

export class VendorModel extends EntityModel
{
  Vendordetails: string;
  BankAccountDetails: string;
  Type: string;
}

import { EntityModel } from 'pck-general-components';

export class VisitModel extends EntityModel
{
  VisitFor: string;
  VisitType: string;
  AppointmentType: string;
  PatientName: string;
}

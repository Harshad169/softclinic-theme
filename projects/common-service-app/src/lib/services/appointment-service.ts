import { AppointmentModel } from "../models/appointment-model";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { CommonModuleRestService } from "./base/common-module-rest-service";
import { Filter, PagedData } from "pck-general-components";
// import { Filter, PagedData } from "pck-api";

@Injectable()
export class AppointmentService extends CommonModuleRestService {
  //private basePath = "CommonModules/api/appointments";
  constructor(@Inject(HttpClient) http: HttpClient) {
    super(http);
  }
  getAppointments(filter: Filter): Promise<HttpResponse<AppointmentModel>> {
    let url = `/appointments?` + filter.getQueryString();
    return super.GetResponse<AppointmentModel>(url);
  }

  remove(id: string) {
    var path = `/appointments/${id}`;
    return this.Delete(path, null);
  }
  // public getList(filter: Filter): Promise<PagedData<AppointmentModel>> {
  //   return this.restService.getResponseWithResolver(this.basePath, filter);
  // }
  // public getdoctorlist(): Promise<CommonDropdownModel[]> {
  //   return this.restService.get<CommonDropdownModel[]>(
  //     `${this.basePath}/GetDoctor`
  //   );
  // }
  // public get(id: string): Promise<AppointmentModel> {
  //   return this.restService.get(`${this.basePath}/${id}`);
  // }

  // public create(entity: AppointmentModel): Promise<AppointmentModel> {
  //   return this.restService.post<AppointmentModel>(this.basePath, entity);
  // }

  // public update(entity: AppointmentModel): Promise<void> {
  //   return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  // }

  // public delete(id: string): Promise<void> {
  //   return this.restService.delete(`${this.basePath}/${id}`);
  // }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

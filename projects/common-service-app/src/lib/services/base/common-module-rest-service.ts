import { HttpClient, HttpResponse } from "@angular/common/http";
import { RestService, RejectedResponse } from "pck-api";

export class CommonModuleRestService extends RestService {
  constructor(public http: HttpClient) {
    super(http, "http://192.168.43.228:16005/commonmodules/api");
  }

  get<T>(path: string): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      super.Get<T>(path).then(  
        (response) => {
          resolve(response);
        },
        (rejected) => {
          reject(this.handleError(rejected));
        }
      );
    });
  }

  getResponse<T>(path: string): Promise<HttpResponse<T>> {
    return new Promise<HttpResponse<T>>((resolve, reject) => {
      super.GetResponse<T>(path).then(
        (response: any) => {
          resolve(response);
        },
        (rejected) => {
          reject(this.handleError(rejected));
        }
      );
    });
  }

  post<T>(path: string, request: any): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      super.Post<T>(path, request).then(
        (response) => {
          resolve(response);
        },
        (rejected) => {
          reject(this.handleError(rejected));
        }
      );
    });
  }

  put(path: string, request: any): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      super.Put<void>(path, request).then(
        (response) => {
          resolve();
        },
        (rejected) => {
          reject(this.handleError(rejected));
        }
      );
    });
  }

  delete(path: string, request: any): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      super.Delete<void>(path, request).then(
        (response) => {
          resolve();
        },
        (rejected) => {
          reject(this.handleError(rejected));
        }
      );
    });
  }

  private handleError(rejected: any): RejectedResponse {
    return new RejectedResponse(rejected);
  }
}

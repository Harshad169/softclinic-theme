import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { CommissionModel } from "../models/commission-model";

export class CommissionService
{
  private basePath = "CommonModules/api/CommissionDetails";
  constructor(private restService: CommonModuleRestService) { }

  public getList(filter: Filter): Promise<PagedData<CommissionModel>>
  {
    return this.restService.getPagedData(this.basePath, filter);
  }

  public get(id: string): Promise<CommissionModel>
  {
    return this.restService.get(`${this.basePath}/${id}`);
  }

  public create(entity: CommissionModel): Promise<CommissionModel>
  {
    debugger;
    return this.restService.post<CommissionModel>(this.basePath, entity);
  }

  public update(entity: CommissionModel): Promise<void>
  {
    return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  }

  public delete(id: string): Promise<void>
  {
    return this.restService.delete(`${this.basePath}/${id}`);
  }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

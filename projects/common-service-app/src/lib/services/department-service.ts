import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { DepartmentModel } from "../models/department-model";

export class DepartmentService
{
  private basePath = "CommonModules/api/departments";
  constructor(private restService: CommonModuleRestService) { }

  // public getList(filter: Filter): Promise<PagedData<DepartmentModel>> {
  //   return this.restService.getPagedData(this.basePath, filter);
  // }

  // public get(id: string): Promise<DepartmentModel> {
  //   return this.restService.get(`${this.basePath}/${id}`);
  // }

  // public create(entity: DepartmentModel): Promise<DepartmentModel> {
  //   return this.restService.post<DepartmentModel>(this.basePath, entity);
  // }

  // public update(entity: DepartmentModel): Promise<void> {
  //   return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  // }

  // public delete(id: string): Promise<void> {
  //   return this.restService.delete(`${this.basePath}/${id}`);
  // }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

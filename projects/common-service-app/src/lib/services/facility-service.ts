import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { FacilityModel } from "../models/facility-model";

export class FacilityService
{
  private basePath = "CommonModules/api/facilitys";
  constructor(private restService: CommonModuleRestService) { }

  // public getList(filter: Filter): Promise<PagedData<FacilityModel>> {
  //   return this.restService.getPagedData(this.basePath, filter);
  // }

  // public get(id: string): Promise<FacilityModel> {
  //   return this.restService.get(`${this.basePath}/${id}`);
  // }

  // public create(entity: FacilityModel): Promise<FacilityModel> {
  //   // debugger;
  //   return this.restService.post<FacilityModel>(this.basePath, entity);
  // }

  // public update(entity: FacilityModel): Promise<void> {
  //   return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  // }

  // public delete(id: string): Promise<void> {
  //   return this.restService.delete(`${this.basePath}/${id}`);
  // }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

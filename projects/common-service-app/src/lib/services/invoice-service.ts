import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { InvoiceModel } from "../models/invoice-model";
import { Filter, PagedData } from "projects/pck-api/src/public-api";

export class InvoiceService {
  private basePath = "api/invoices";
  constructor(private restService: CommonModuleRestService) {}

  public getList(filter: Filter): Promise<PagedData<InvoiceModel>> {
    return this.restService.getPagedData(this.basePath, filter);
  }

  public get(id: string): Promise<InvoiceModel> {
    return this.restService.get(`${this.basePath}/${id}`);
  }

  public create(entity: InvoiceModel): Promise<InvoiceModel> {
    return this.restService.post<InvoiceModel>(this.basePath, entity);
  }

  public update(entity: InvoiceModel): Promise<void> {
    return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  }

  public delete(id: string): Promise<void> {
    return this.restService.delete(`${this.basePath}/${id}`);
  }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

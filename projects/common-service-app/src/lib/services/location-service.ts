import { LocationModel } from "../models/location-model";
import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Filter } from "pck-general-components";

@Injectable()
export class LocationService extends CommonModuleRestService {
  constructor(@Inject(HttpClient) http: HttpClient) {
    super(http);
  }

  getLocations(filter: Filter): Promise<HttpResponse<LocationModel>> {
    let url = `/locations?` + filter.getQueryString();
    return super.GetResponse<LocationModel>(url);
  }

  remove(id: string) {
    var path = `/locations/${id}`;
    return this.Delete(path, null);
  }
  // public get(id: string): Promise<LocationRequestModel> {
  //   return super.get(`${this.basePath}/${id}`);
  // }
  // // public getList(filter: Filter): Promise<PagedData<LocationModel>> {
  // //   return this.restService.getPagedData(this.basePath, filter);
  // // }

  // // public get(id: string): Promise<LocationRequestModel> {
  // //   return super.Get(`${this.basePath}/${id}`);
  // // }

  // public getdropdownlist(): Promise<CommonDropdownModel[]> {
  //   return this.restService.get<CommonDropdownModel[]>(
  //     `${this.basePath}/GetLocationCode`
  //   );
  // }
  // public getlocationtypelist(): Promise<CommonDropdownModel[]> {
  //   return this.restService.get<CommonDropdownModel[]>(
  //     `${this.basePath}/GetLocationType`
  //   );
  // }
  // public create(entity: LocationRequestModel): Promise<LocationRequestModel> {
  //   return this.restService.post<LocationRequestModel>(this.basePath, entity);
  // }

  // public update(entity: LocationRequestModel): Promise<void> {
  //   return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  // }

  // public delete(id: string): Promise<void> {
  //   return this.restService.delete(`${this.basePath}/${id}`);
  // }

  // public getLocations(): Promise<LocationModel[]> {
  //   return this.restService.get<LocationModel[]>(`${this.basePath}/locations`);
  // }
}

import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { ServiceUsageModel } from "../models/service-usage-model";
import { Filter, PagedData } from "projects/pck-api/src/public-api";

export class ServiceUsageService {
  private basePath = "CommonModules/api/serviceusages";
  constructor(private restService: CommonModuleRestService) {}

  public getList(filter: Filter): Promise<PagedData<ServiceUsageModel>> {
    return this.restService.getPagedData(this.basePath, filter);
  }

  public get(id: string): Promise<ServiceUsageModel> {
    return this.restService.get(`${this.basePath}/${id}`);
  }

  public create(entity: ServiceUsageModel): Promise<ServiceUsageModel> {
    return this.restService.post<ServiceUsageModel>(this.basePath, entity);
  }

  public update(entity: ServiceUsageModel): Promise<void> {
    return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  }

  public delete(id: string): Promise<void> {
    return this.restService.delete(`${this.basePath}/${id}`);
  }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

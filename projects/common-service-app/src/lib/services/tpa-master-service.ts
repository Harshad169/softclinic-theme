import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { TpaMasterModel } from "../models/tpa-master-model";

export class TpaMasterService
{
  private basePath = "CommonModules/api/tpaMasters";
  constructor(private restService: CommonModuleRestService) { }

  // public getList(filter: Filter): Promise<PagedData<TpaMasterModel>> {
  //   return this.restService.getPagedData(this.basePath, filter);
  // }

  // public get(id: string): Promise<TpaMasterModel> {
  //   return this.restService.get(`${this.basePath}/${id}`);
  // }

  // public create(entity: TpaMasterModel): Promise<TpaMasterModel> {
  //   return this.restService.post<TpaMasterModel>(this.basePath, entity);
  // }

  // public update(entity: TpaMasterModel): Promise<void> {
  //   return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  // }

  // public delete(id: string): Promise<void> {
  //   return this.restService.delete(`${this.basePath}/${id}`);
  // }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

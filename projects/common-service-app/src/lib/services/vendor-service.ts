import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { VendorModel } from "../models/vendor-model";

export class VendorService
{
  private basePath = "CommonModules/api/vendors";
  constructor(private restService: CommonModuleRestService) { }

  // public getList(filter: Filter): Promise<PagedData<VendorModel>> {
  //   return this.restService.getPagedData(this.basePath, filter);
  // }

  // public get(id: string): Promise<VendorModel> {
  //   return this.restService.get(`${this.basePath}/${id}`);
  // }

  // public create(entity: VendorModel): Promise<VendorModel> {
  //   return this.restService.post<VendorModel>(this.basePath, entity);
  // }

  // public update(entity: VendorModel): Promise<void> {
  //   return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  // }

  // public delete(id: string): Promise<void> {
  //   return this.restService.delete(`${this.basePath}/${id}`);
  // }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

import { CommonModuleRestService } from "projects/common-service-app/src/lib/services/base/common-module-rest-service";
import { VisitModel } from "../models/visit-model";

export class VisitService
{
  private basePath = "CommonModules/api/visits";
  constructor(private restService: CommonModuleRestService) { }

  // public getList(filter: Filter): Promise<PagedData<VisitModel>> {
  //   return this.restService.getPagedData(this.basePath, filter);
  // }

  // public get(id: string): Promise<VisitModel> {
  //   return this.restService.get(`${this.basePath}/${id}`);
  // }

  // public create(entity: VisitModel): Promise<VisitModel> {
  //   return this.restService.post<VisitModel>(this.basePath, entity);
  // }

  // public update(entity: VisitModel): Promise<void> {
  //   return this.restService.put(`${this.basePath}/${entity.id}`, entity);
  // }

  // public delete(id: string): Promise<void> {
  //   return this.restService.delete(`${this.basePath}/${id}`);
  // }

  // public getMedicines(): Promise<MedicineModel[]> {
  //     return this.restService.get<MedicineModel[]>(`${this.basePath}/medicines`);
  // }
}

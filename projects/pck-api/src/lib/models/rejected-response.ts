// import { ErrorResponse } from "./error-response";

export class RejectedResponse {
  statusCode: number;
  //errors: ErrorResponse[];
  rejected: any;

  constructor(rejected: any) {
    if (rejected) {
      this.statusCode = rejected.statusCode;
      rejected.errors = rejected.errors || [];

      //   this.errors = [];
      //   for (const error of rejected.errors) {
      //     if (error.message)
      //       this.errors.push(new ErrorResponse(error.code, error.message));
      //   }
      this.rejected = rejected;
    }
  }
}

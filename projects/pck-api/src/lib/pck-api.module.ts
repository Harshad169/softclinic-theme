import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule
  ],
  exports: []
})
export class PckApiModule
{

  constructor(@Optional() @SkipSelf() parentModule: PckApiModule)
  {
    if (parentModule)
      console.log('PckApiModule loaded');
  }

  static forRoot(): ModuleWithProviders
  {
    return {
      ngModule: PckApiModule,
      providers: []
    };
  }

  static forChild(): ModuleWithProviders
  {
    return {
      ngModule: PckApiModule,
    };
  }
}

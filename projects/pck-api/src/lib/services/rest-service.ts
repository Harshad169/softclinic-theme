
import { throwError as observableThrowError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

export class RestService {
    private headerCollection: HttpHeaders;
    public baseUrl = '#';

    constructor(protected http: HttpClient, baseUrl: string, header?: HttpHeaders) {
        this.baseUrl = baseUrl;
        this.headerCollection = new HttpHeaders();
        if (header)
            this.headerCollection = header;
    }

    protected Get<T>(path: string): Promise<T> {
        console.log('GET:', this.url(path));

        return this.http.get<T>(this.url(path), { headers: this.headers() }).pipe(
            tap((res) => {
                console.log('-> Result', res);
            }),
            catchError((error) => {
                return observableThrowError(this.errorMessages(error));
            }))
            .toPromise();
    }

    protected GetResponse<T>(path: string): Promise<HttpResponse<T>> {
        console.log('GET RESPONSE:', this.url(path));
        return this.http.get<T>(this.url(path), { headers: this.headers(), observe: 'response' }).pipe(
            map((res) => {
                console.log('-> Result', res);
                console.log('-> Headers', res.headers);
                return res;
            }),
            catchError((error) => {
                return observableThrowError(this.errorMessages(error));
            }))
            .toPromise();
    }

    protected Post<T>(path: string, request: any): Promise<T> {
        console.log('POST:', this.url(path), request);
        const body = JSON.stringify(request);

        return this.http.post<T>(this.url(path), body, { headers: this.headers() }).pipe(
            tap((res) => {
                console.log('-> Result', res);
            }), catchError((error: any) => {
                return observableThrowError(this.errorMessages(error));
            }))
            .toPromise();
    }

    protected Put<T>(path: string, request: any): Promise<T> {
        console.log('PUT:', this.url(path), request);
        const body = JSON.stringify(request);
        return this.http.put<T>(this.url(path), body, { headers: this.headers() }).pipe(
            tap((res) => {
                console.log('-> Result', res);
            }), catchError((error: any) => {
                return observableThrowError(this.errorMessages(error));
            }))
            .toPromise();
    }

    protected Delete<T>(path: string, request: any): Promise<T> {
        console.log('DELETE:', this.url(path), request);

        return this.http.request<T>('delete', this.url(path), { body: request, headers: this.headers() }).pipe(
            map((res) => {
                console.log('-> Result', res);
                return res;
            }), catchError((error: any) => {
                return observableThrowError(this.errorMessages(error));
            }))
            .toPromise();
    }

    protected headers(): HttpHeaders {
        const headers = this.headerCollection
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*');
        return headers;
    }

    protected url(path: string): string {
        const absoluteCheck = /^https?:\/\//i;
        return (absoluteCheck.test(path)) ? path : this.baseUrl + path;
    }

    protected errorMessages(response: any) {
        const error = (response.error) ? response.error : this.errorByCode(response.status);
        error.statusCode = response.status;
        console.log(`-> ERROR ${response.status}`, response.status ? error : response);
        return error;
    }

    protected errorByCode(errorCode: number) {
        switch (errorCode) {
            case 403:
                return { errors: [{ message: 'Forbidden', code: 403 }] };
            case 500:
                return { errors: [{ message: 'Internal Server Error', code: 500 }] };
            case 502:
                return { errors: [{ message: 'Bad gateway, please try again later', code: 502 }] };
            case 404:
                return { errors: [{ message: 'Page not found. Check if you\'re using the correct ID', code: 404 }] };
            default:
                return { errors: [{ message: 'Something went wrong', code: errorCode }] };
        }
    }
}

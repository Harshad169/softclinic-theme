import { Injectable } from "@angular/core";
import { LANGUAGES } from "../Models/language-configuration";
import { LanguageModel } from "../Models/language.model";
import { TranslationService, LocaleService } from "angular-l10n";

@Injectable()
export class ResourceTranslationService {
  constructor(
    private translationService: TranslationService,
    private localeService: LocaleService
  ) {}

  public get languages(): LanguageModel[] {
    return LANGUAGES;
  }

  public get currentLanguage(): string {
    return this.localeService.getCurrentLanguage();
  }

  public setLanguage(language: LanguageModel) {
    this.localeService.setCurrentLanguage(language.code);
  }

  public translate(
    keys: string | string[],
    args?: any,
    lang?: string
  ): string | any {
    return this.translationService.translate(keys, args);
  }
}

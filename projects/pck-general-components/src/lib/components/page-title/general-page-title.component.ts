import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "general-page-title",
  templateUrl: "./general-page-title.component.html",
})
export class GeneralPageTitleComponent {
  @Input() heading;
  @Input() subheading;
  @Input() icon;

  @Input() createButton: boolean = false;
  @Input() backButton: boolean = false;

  @Output()
  onCreateClick = new EventEmitter();
  @Output()
  onBackClick = new EventEmitter();

  onCreate(event) {
    this.onCreateClick.emit(event);
  }

  onBack(event) {
    this.onBackClick.emit(event);
  }
}

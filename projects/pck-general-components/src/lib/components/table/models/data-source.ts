import { Filter } from './filter';
import { Subject, Observable } from 'rxjs';

export class DataSource<T> extends Subject<Array<T>>
{
  public data: any[] = [];
  public totalCount: number = 0;
  public filter: Filter = new Filter(1);

  private filterData: (filter: Filter) => Promise<any> | Observable<any>;
  public fetching = false;

  public bind(fetchData: (filter: Filter) => Promise<any> | Observable<any>)
  {
    this.filterData = fetchData;
  }

  public rebind(): Promise<Array<T>>
  {
    this.setData([], 0);
    return this.fetchData();
  }

  private fetchData(): Promise<Array<T>>
  {
    const promise = new Promise<any>((resolve, reject) =>
    {
      if (!this.filterData)
      {
        reject('No fetchdata function has been set on the datasource');
        return;
      }

      if (this.fetching)
      {
        reject('The datasource is already fetching data');
        return;
      }

      this.fetching = true;

      const promiseOrObservable = this.filterData(this.filter);
      if (promiseOrObservable instanceof Promise)
      {
        promiseOrObservable.then(response =>
        {
          let data: Array<any> = [];
          let totalCount: number = 0;
          if (response && response.headers && response.body)
          {
            data = (!response.body) ? [] : response.body;
            if (response.headers.has("TotalCount"))
            {
              totalCount = +response.headers.get("TotalCount");
            }
          }
          else
            data = response;

          this.setData(data, totalCount);
          this.fetching = false;
          resolve(data as T[]);
        }).catch(e =>
        {
          console.error('Failed to bind in datasource', e);
          const data: Array<any> = [];
          this.setData(data, 0);
          this.fetching = false;
          resolve(data as T[]);
        });
      }
      else
      {
        promiseOrObservable.subscribe(data =>
        {
          this.setData(data, 0);
          this.fetching = false;
          resolve(data as T[]);
        });
      }
    });
    return promise;
  }

  private setData(data: Array<any>, totalCount: number)
  {
    this.data = data;
    this.totalCount = totalCount;
  }
}

export class Filter
{
  page: number;
  perPage: number;
  orderBy: string;
  direction: "asc" | "desc";
  searchColumns: string[];
  searchValue: string;

  constructor(
    page: number,
    perPage: number = 10,
    orderBy: string = "",
    direction: "asc" | "desc" = "asc",
    searchColumns: string[] = null,
    searchValue = ""
  )
  {
    this.page = page;
    this.perPage = perPage;
    this.orderBy = orderBy;
    this.direction = direction;
    this.searchColumns = searchColumns;
    this.searchValue = searchValue;
  }

  public getQueryString(): string
  {
    let queryString = "";
    queryString = this.addQueryParam(queryString, "page", this.page);
    queryString = this.addQueryParam(queryString, "perpage", this.perPage);
    queryString = this.addQueryParam(queryString, "orderby", this.orderBy);
    queryString = this.addQueryParam(queryString, "direction", this.direction);
    queryString = this.addQueryParam(
      queryString,
      "searchValue",
      this.searchValue
    );

    if (this.searchColumns && this.searchColumns.length > 0)
    {
      queryString = this.addQueryParam(
        queryString,
        "searchColumns",
        this.searchColumns.join()
      );
    }
    return queryString;
  }

  private addQueryParam(queryString: string, key: string, value: any): string
  {
    if (value)
    {
      if (queryString.indexOf("=") > 0) queryString += "&";

      queryString += `${key}=${value}`;
    }
    return queryString;
  }
}

export class PagedData<T> {
    data: T[];
    page: number;
    totalCount: number;

    constructor(data: T[], page: number, totalCount: number) {
        this.data = data;
        this.page = page;
        this.totalCount = totalCount;
    }
}

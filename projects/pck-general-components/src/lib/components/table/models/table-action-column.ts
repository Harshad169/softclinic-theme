export class TableActionColumn {
  title: string;
  icon: string;
  click?: (row: any) => void = null;
}

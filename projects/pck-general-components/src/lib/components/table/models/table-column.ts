export class TableColumn {
    label: string;
    //width: string;
    visible?: boolean = true;
    orderBy?: string = "";
    cssClass?: string = "";
    name: string;
    isDetail?: boolean = false;
    isFilterAllowed?: boolean = false;
    isDefaultColumn?: boolean = false;
    render?: (row: any) => string = null;
}

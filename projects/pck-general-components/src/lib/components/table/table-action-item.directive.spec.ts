// import {
//   Component,
//   OnInit,
//   Input,
//   TemplateRef,
//   ContentChild,
//   Directive,
//   ElementRef,
//   ChangeDetectorRef,
//   SimpleChanges,
//   EventEmitter,
//   Output
// } from "@angular/core";
// import { SCTableComponent } from "./table.component";
// import { SCTableActionColumnDirective } from "./table-action-column.directive.spec";
// //import { SCTableActionColumnDirective } from './table-action-column.directive';

// @Directive({
//   selector: "sc-table-action-item"
// })
// export class SCTableActionItemDirective {
//   constructor(
//     private tableActionColumnComponent: SCTableActionColumnDirective
//   ) {
//     tableActionColumnComponent.addItem(this);
//   }

//   @Input()
//   title: string;
//   @Input()
//   icon: string;

//   @Output()
//   onClick: EventEmitter<any> = new EventEmitter<any>();

//   onItemClick(row: any) {
//     this.onClick.emit(row);
//   }
// }

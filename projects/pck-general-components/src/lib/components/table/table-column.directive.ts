import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  ContentChild,
  Directive,
  ElementRef,
  ChangeDetectorRef,
  SimpleChanges
} from "@angular/core";
import { SCTableComponent } from "./table.component";

@Directive({
  selector: "sc-table-column"
})
export class SCTableColumnDirective {
  constructor(
    private tableComponent: SCTableComponent,
    public element: ElementRef,
    private changeDetection: ChangeDetectorRef
  ) {
    tableComponent.addColumn(this);
  }

  @ContentChild(TemplateRef, { static: false })
  public template: TemplateRef<any>;

  @Input()
  title: string;
  @Input()
  icon: string;

  @Input()
  orderBy: string;

  @Input()
  visible: boolean = true;

  @Input()
  displayColumns: Array<{ title: string; value: string }> = [];

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let change = changes[propName];
      if (change && !change.isFirstChange()) {
        this.tableComponent.refreshView();
        break;
      }
    }
  }
}

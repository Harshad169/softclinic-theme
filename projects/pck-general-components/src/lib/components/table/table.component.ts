import
{
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
  EventEmitter,
  Output,
  OnDestroy,
  ViewChild
} from "@angular/core";
import { SCTableColumnDirective } from "./table-column.directive";
import { SCTableActionColumnDirective } from "./table-action-column.directive";
import { FormControl } from "@angular/forms";
import { DataSource } from "./models/data-source";
import { TableColumn } from "./models/table-column";
import { TableActionColumn } from "./models/table-action-column";

@Component({
  selector: "sc-table",
  templateUrl: "table.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SCTableComponent implements OnInit, OnDestroy
{
  //@ViewChild('ddlSelectColumns') ddlSelectColumns;
  @ViewChild("ddlSelectColumns", { static: false }) ddlSelectColumns;
  //@Language() lang: string;

  constructor(private changeDetection: ChangeDetectorRef) { }
  columns: SCTableColumnDirective[] = [];
  actionColumn: SCTableActionColumnDirective = null;

  // @Input()
  // datasource: DataSource = new DataSource();
  @Input()
  dataSource: DataSource<any>;

  // @Input()
  // public data: Array<any> = [];

  @Input()
  columnInfo: TableColumn[] = [];
  @Input()
  actionColumnsList: TableActionColumn[] = [];

  primaryColumns: TableColumn[] = [];
  detailColumns: TableColumn[] = [];
  selectedColumnItems: TableColumn[] = [];
  filterColumns: string[] = [];

  @Output()
  onRowClick: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  public orderBy: string;
  selectColumnList = new FormControl();
  public searchTerm: string = "";
  //toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  public direction: "asc" | "desc" = "asc";
  public perPage: number = 10;
  private _page: number = 1;
  public get page(): number
  {
    return this._page;
  }
  public set page(value: number)
  {
    this._page = value;
    this.renderData();
  }

  ngOnInit()
  {
    if (!this.dataSource)
      console.log('No datasource or data has been set on the table!');

    // this.dataSource.subscribe(data =>
    // {
    //   this.data = data;
    //   this.detectChanges();
    // });

    this.prepareColumns();
    this.renderData();
  }

  ngOnDestroy() 
  {
    if (!this.dataSource)
      return;
    // this.dataSource.unsubscribe();
  }

  private prepareColumns()
  {
    //alert("Inside Prepare Columns");
    this.primaryColumns = [];
    this.filterColumns = [];
    this.detailColumns = [];
    this.selectedColumnItems = [];
    //this.primaryColumns = this.columnInfo.filter(column => !column.isDetail);

    this.columnInfo
      .filter(column => !column.isDetail)
      .forEach(element =>
      {
        //this.primaryColumns.push(element);
        let pridetail = Object.assign([], element);
        this.primaryColumns.push(pridetail);
      });

    if (this.actionColumnsList.length > 0)
    {
      this.primaryColumns.push({
        label: "Action",
        name: "ActionColumn",
        orderBy: "",
        isFilterAllowed: false,
        visible: true,
        isDefaultColumn: false
      });
    }
    //this.detailColumns=this.columnInfo.filter(column => column.isDetail === true);
    this.columnInfo
      .filter(column => column.isDetail === true)
      .forEach(element =>
      {
        //this.detailColumns.push(element);
        let detdetail = Object.assign([], element);
        this.detailColumns.push(detdetail);
      });
    //this.detailColumns = Object.assign([], this.columnInfoMain.filter(column => column.isDetail === true));
    this.columnInfo
      .filter(column => column.isFilterAllowed === true)
      .forEach(element =>
      {
        this.filterColumns.push(element.name);
      });
    this.selectedColumnItems = this.columnInfo.filter(
      column => column.visible === true || column.isDefaultColumn === true
    );
  }

  public addColumn(column: SCTableColumnDirective)
  {
    this.columns.push(column);
  }

  public refresh()
  {
    this.renderData();
  }

  public refreshView()
  {
    this.detectChanges();
  }

  private detectChanges()
  {
    this.changeDetection.detectChanges();
  }
  onSearch()
  {
    this.renderData();
  }
  onApplyColumn()
  {
    // alert("Apply Column Filter");

    this.primaryColumns.forEach(element =>
    {
      if (element.name == "ActionColumn")
      {
        element.visible = true;
      } else
      {
        var pricol = this.selectColumnList.value.filter(
          col => col.name == element.name
        );
        if (pricol.length > 0)
        {
          element.visible = true;
        } else
        {
          element.visible = false;
        }
      }
    });
    this.detailColumns.forEach(element =>
    {
      var detcol = this.selectColumnList.value.filter(
        col => col.name == element.name
      );
      if (detcol.length > 0)
      {
        element.visible = true;
      } else
      {
        element.visible = false;
      }
    });

    this.ddlSelectColumns.toggle();
    this.renderData();
  }
  onClearColumn()
  {
    // alert("Clear Filter");
    this.prepareColumns();

    this.ddlSelectColumns.toggle();
    this.renderData();
  }

  private renderData()
  {
    this.dataSource.filter.orderBy = this.orderBy;
    this.dataSource.filter.direction = this.direction;
    this.dataSource.filter.page = this.page;
    this.dataSource.filter.perPage = this.perPage;
    if (this.searchTerm != "")
    {
      this.dataSource.filter.searchColumns = this.filterColumns;
      this.dataSource.filter.searchValue = this.searchTerm;
    } else
    {
      this.dataSource.filter.searchColumns = [];
      this.dataSource.filter.searchValue = "";
    }
    this.dataSource.rebind().then(() =>
    {
      this.detectChanges();
    });
  }


  private resetPage()
  {
    this._page = 1;
  }

  private changeDirection(reset: boolean = false)
  {
    if (reset === true)
    {
      this.direction = "asc";
    } else
    {
      this.direction = this.direction == "asc" ? "desc" : "asc";
    }
  }
  OnParentClick(divid)
  {
    var ele = document.getElementsByClassName("div_" + divid);
    for (var i = 0; i < ele.length; i++)
    {
      if (document.getElementById(ele[i].id).style.display == "none")
      {
        document.getElementById(ele[i].id).style.display = "table-row";
      } else
      {
        document.getElementById(ele[i].id).style.display = "none";
      }
    }
  }

  rowClick(row: any)
  {
    this.onRowClick.emit(row);
  }

  onActionItemClick(item: TableActionColumn, row: any)
  {
    //alert(row);
    item.click(row);
    //item.onItemClick(row);
  }
}

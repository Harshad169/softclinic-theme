export class control {
    id: string;
    parentid: string;
    icon: string;
    url: string;
    title: string;
    Subcontrol?: Array<Subcontrol>

}
export class submenu {
    icon: string;
    url: string;
    title: string;
    Subcontrol?: Array<Subcontrol>

}
export class Subcontrol {
    id: string;
    parentid: string;
    icon: string;
    url: string;
    title: string;

}
export class LocationControl {
    LocationType: string;
    Id: string;
    isFacility: string;
    isGroup: string;
    LocationTypeCode: string;
}



import { Language } from 'angular-l10n/src/models/types';

export class LanguageModel implements Language
{
    code: string;
    dir: string;
    englishName: string;
}

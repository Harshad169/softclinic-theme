import {
  Component,
  HostListener,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
} from "@angular/core";
import { ThemeOptions } from "../../../theme-options";
import { select } from "@angular-redux/store";
import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { control } from "projects/pck-general-components/src/lib/models/control";
import { AccordionComponent } from "src/app/accordion/accordion.component";
@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
})
export class SidebarComponent implements OnInit {
  @ViewChild("viewContainerRef", { static: true, read: ViewContainerRef })
  VCR: ViewContainerRef;

  public extraParameter: any;
  public selectmenu: string;
  public mainMenu: Array<control> = [
    {
      icon: "vsm-icon pe-7s-safe",
      id: "6",
      parentid: "0",
      title: "DASHBOARD",
      url: "/Widget/details",
    },
    {
      icon: "vsm-icon pe-7s-rocket",
      id: "1",
      parentid: "0",
      title: "ACTIVITY",
      url: "",
    },
    {
      icon: "vsm-icon pe-7s-monitor",
      id: "2",
      parentid: "0",
      title: "OPD",
      url: "",
    },
    {
      icon: "vsm-icon pe-7s-users",
      id: "3",
      parentid: "0",
      title: "LAB",
      url: "",
    },
    {
      icon: "vsm-icon pe-7s-link",
      id: "4",
      parentid: "0",
      title: "SYSTEM-MASTER",
      url: "",
    },
    {
      icon: "vsm-icon pe-7s-safe",
      id: "5",
      parentid: "0",
      title: "SYSTEM-MANAGEMENT",
      url: "",
    },
    {
      icon: "vsm-icon pe-7s-safe",
      id: "6",
      parentid: "0",
      title: "CUSTOM",
      url: "",
    },
  ];

  @select("config") public config$: Observable<any>;
  private newInnerWidth: number;
  private innerWidth: number;
  activeId = "dashboardsMenu";
  componentsReferences = [];

  constructor(
    private CFR: ComponentFactoryResolver,
    public globals: ThemeOptions,
    private activatedRoute: ActivatedRoute
  ) {}

  createComponent(params: string) {
    let _url = this.mainMenu.find((x) => x.title == params).url;
    if (_url == "") {
      this.selectmenu = params;
      document.getElementById("divselectmenu").style.display = "block";
      document.getElementById("divselectmenu").style.visibility = "Visible";
      var x = document
        .getElementById("divmainMenu")
        .getElementsByClassName("vsm-title");
      for (var i = 0; i < x.length; i++) {
        let htmleEl = x[i] as HTMLElement;
        htmleEl.style.display = "none";
      }
      // Clear previous bind html
      this.VCR.clear();
      //set static parentId
      AccordionComponent.ParentId = this.mainMenu.find(
        (x) => x.title == params
      ).id;

      let componentFactory = this.CFR.resolveComponentFactory(
        AccordionComponent
      );
      let componentRef: ComponentRef<AccordionComponent> = this.VCR.createComponent(
        componentFactory
      );
      // add reference for newly created component
      this.componentsReferences.push(componentRef);
    }
  }

  toggleSidebar() {
    this.globals.toggleSidebar = !this.globals.toggleSidebar;
  }

  sidebarHover() {
    this.globals.sidebarHover = !this.globals.sidebarHover;
  }

  ngOnInit() {
    setTimeout(() => {
      this.innerWidth = window.innerWidth;
      if (this.innerWidth < 1200) {
        this.globals.toggleSidebar = true;
      }
    });

    this.extraParameter = this.activatedRoute.snapshot.firstChild.data.extraParameter;
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.newInnerWidth = event.target.innerWidth;

    if (this.newInnerWidth < 1200) {
      this.globals.toggleSidebar = true;
    } else {
      this.globals.toggleSidebar = false;
    }
  }

  menuclick(event, params: string) {
    this.selectmenu = params;
    document.getElementById("divselectmenu").style.display = "block";
    document.getElementById("divselectmenu").style.visibility = "Visible";
    var x = document.getElementsByClassName("vsm-title");
    for (var i = 0; i < x.length; i++) {
      let htmleEl = x[i] as HTMLElement;
      htmleEl.style.display = "none";
    }
    //const factory = cfr.resolveComponentFactory(AccordionComponent);
  }

  backMainMenuClick() {
    this.selectmenu = "";
    document.getElementById("divselectmenu").style.display = "none";
    document.getElementById("divselectmenu").style.visibility = "Hidden";
    var x = document.getElementsByClassName("vsm-title");
    for (var i = 0; i < x.length; i++) {
      let htmleEl = x[i] as HTMLElement;
      htmleEl.style.display = "block";
    }
  }
}

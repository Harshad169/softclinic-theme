import { Component, OnInit } from "@angular/core";
import { ResourceTranslationService } from "../Services/resource-translation.service";
import { LanguageModel } from "../Models/language.model";

@Component({
  selector: "sc-language-selection",
  templateUrl: "language-selection.component.html",
})
export class SCLanguageSelectionComponent implements OnInit {
  public languages: LanguageModel[] = [];
  public currentLanguage: LanguageModel = new LanguageModel();
  constructor(private translation: ResourceTranslationService) {}

  ngOnInit() {
    //  this.languages = this.translation.languages;
    this.setCurrentLanguage();
  }

  setLanguage(language: LanguageModel) {
    // this.translation.setLanguage(language);
    this.setCurrentLanguage();
  }

  private setCurrentLanguage() {
    //this.currentLanguage = this.languages.find(item => item.code === this.translation.currentLanguage);
  }
}

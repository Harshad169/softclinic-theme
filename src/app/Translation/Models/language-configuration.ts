// import { LanguageModel } from './language.model';
import { LanguageModel } from "./language.model";

export const LANGUAGES: LanguageModel[] = [
  { code: "en", dir: "ltr", englishName: "English" },
  { code: "fr", dir: "ltr", englishName: "French" },
];

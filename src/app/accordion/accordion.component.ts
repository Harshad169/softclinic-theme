import { Component, OnInit } from "@angular/core";
import { control } from "projects/pck-general-components/src/lib/models/control";

@Component({
  selector: "gc-accordion",
  templateUrl: "./accordion.component.html",
  styleUrls: ["./accordion.component.css"],
})
export class AccordionComponent implements OnInit {
  public submenu: Array<control> = [
    {
      icon: "",
      title: "Appointment",
      id: "2",
      parentid: "1",
      url: "/common/appointments",
    },
    {
      icon: "",
      title: "Petient",
      id: "3",
      parentid: "1",
      url: "/medicine/list",
    },
    {
      icon: "",
      title: "IntakeOutPut",
      id: "3",
      parentid: "2",
      url: "/intakeoutput/list",
    },
    {
      icon: "",
      title: "Investigation",
      id: "4",
      parentid: "3",
      url: "/innvestigation/list",
    },
    {
      icon: "",
      title: "Service",
      id: "5",
      parentid: "4",
      url: "common/services",
    },
    {
      icon: "",
      title: "Organisation",
      id: "6",
      parentid: "5",
      url: "",
      Subcontrol: [
        {
          icon: "",
          title: "BusinessAccount",
          id: "6.1",
          parentid: "3",
          url: "/buisnessaccount/list",
        },
        {
          icon: "",
          title: "Location",
          id: "6.1",
          parentid: "3",
          url: "/common/locations",
        },
        {
          icon: "",
          title: "Facility",
          id: "6.1",
          parentid: "3",
          url: "common/facilitys",
        },
      ],
    },
    {
      icon: "",
      title: "User",
      id: "6",
      parentid: "5",
      url: "",
      Subcontrol: [
        {
          icon: "",
          title: "User",
          id: "6.1",
          parentid: "3",
          url: "/user/list",
        },
        {
          icon: "",
          title: "Role",
          id: "6.1",
          parentid: "3",
          url: "/role/list",
        },
      ],
    },
    {
      icon: "",
      title: "Department",
      id: "7",
      parentid: "6",
      url: "common/departments",
    },
    {
      icon: "",
      title: "CommissionDetail",
      id: "8",
      parentid: "6",
      url: "common/commissions",
    },
    {
      icon: "",
      title: "ServiceUsage",
      id: "9",
      parentid: "6",
      url: "common/serviceusages",
    },
    {
      icon: "",
      title: "TPAMaster",
      id: "10",
      parentid: "6",
      url: "common/tpamasters",
    },
    {
      icon: "",
      title: "Vender",
      id: "10",
      parentid: "6",
      url: "common/vendors",
    },
    {
      icon: "",
      title: "Visit",
      id: "11",
      parentid: "6",
      url: "common/visits",
    },

    //serviceusage { icon: "", title: "WidgetDetails", id: "12", parentid: "6", url: "/Widget/details" },
  ];
  ParentMenu: control[];
  submenufilter: Array<control>;
  static ParentId: string;
  constructor() {
    this.submenufilter = this.submenu.filter(
      (t) => t.parentid == AccordionComponent.ParentId
    );
  }

  ngOnInit() { }
}

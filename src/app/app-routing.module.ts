import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BaseLayoutComponent } from "./Layout/base-layout/base-layout.component";

const routes: Routes = [
  {
    path: "",
    component: BaseLayoutComponent,
    children: [
      {
        path: "common",
        loadChildren: () =>
          import(
            "./../../projects/common-service-app/src/lib/common-service-app.module"
          ).then((m) => m.CommonServiceAppModule),
      }
    ]
  },
  { path: "**", redirectTo: "common/appointents" },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: "enabled",
      anchorScrolling: "enabled",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
